package com.dimingo.modbus.hao.tcp;

import net.wimpi.modbus.net.TCPMasterConnection;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class ModbusTcpDriverTest {
	
	private ModbusTcpDriver modbusTcpDriver;

	@Before
	public void setUp() {

		System.setProperty("net.wimpi.modbus.debug", "true");

		modbusTcpDriver = new ModbusTcpDriver();

//		modbusTcpDriver.setSlaveIpAddress("192.168.0.232");
//		modbusTcpDriver.setSlaveIpAddress("127.0.0.1");
//		modbusTcpDriver.setSlaveIpAddress("192.168.100.102");
		modbusTcpDriver.setSlaveIpAddress("192.168.0.110");
		modbusTcpDriver.setSlavePort(502);
		modbusTcpDriver.setTransactionDefaultMaxRetries(3);
		modbusTcpDriver.setMasterIdentifier(7);

	} // end setUp
	
	
	@Test
	public void testSetMasterIdentifier() {
		
		modbusTcpDriver.setMasterIdentifier(7);
		
		assertEquals(7, modbusTcpDriver.getMasterIdentifier());
		
	} // end testSetMasterIdentifier
	

	@Test
	public void testOpenConnection() {

		TCPMasterConnection tcpMasterConnection = null;
		
		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		
		assertNotNull(tcpMasterConnection);
		assertTrue(tcpMasterConnection.isConnected());
		
		modbusTcpDriver.closeConnection();
		
	} // end testOpenConnection

	
	@Test
	public void testCloseConnection() {

		TCPMasterConnection tcpMasterConnection = null;
		
		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();
			
		} catch (Exception e) {

			e.printStackTrace();
			
		}
		
		assertNotNull(tcpMasterConnection);
		
		modbusTcpDriver.closeConnection();

		assertNull(modbusTcpDriver.getTcpMasterConnection());

	} // end testCloseConnection


	@Ignore
	@Test
	public void testReadInputRegisters() {
		fail("Not yet implemented");
	}


	@Test
	public void testWriteHoldingRegisters() {

		TCPMasterConnection tcpMasterConnection= null;
		
		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();
			
		} catch (Exception e) {

			e.printStackTrace();
			
		} 
		
		assertNotNull(tcpMasterConnection);
		
		int[] registerValues = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 14 };
		
//		modbusTcpDriver.writeHoldingRegisters(tcpMasterConnection, 1, 9, registerValues);
		modbusTcpDriver.writeHoldingRegisters(1, 0, registerValues);

		modbusTcpDriver.closeConnection();
		
	} // end testWriteHoldingRegisters


	@Test
	public void testWriteHoldingRegistersAllZero() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int[] registerValues = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

//		modbusTcpDriver.writeHoldingRegisters(tcpMasterConnection, 1, 9, registerValues);
		modbusTcpDriver.writeHoldingRegisters(1, 0, registerValues);

		modbusTcpDriver.closeConnection();

	} // end testWriteHoldingRegistersAllZero


	@Test
	public void testWriteHoldingRegistersAllGreen() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int[] registerValues = new int[] { 255, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

//		modbusTcpDriver.writeHoldingRegisters(tcpMasterConnection, 1, 9, registerValues);
		modbusTcpDriver.writeHoldingRegisters(1, 0, registerValues);

		modbusTcpDriver.closeConnection();

	} // end testWriteHoldingRegistersAllGreen


	@Test
	public void testWriteHoldingRegistersAlarm1Red() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int[] registerValues = new int[] { 255, 255, 255, 255, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

//		modbusTcpDriver.writeHoldingRegisters(tcpMasterConnection, 1, 9, registerValues);
		modbusTcpDriver.writeHoldingRegisters(1, 0, registerValues);

		modbusTcpDriver.closeConnection();

	} // end testWriteHoldingRegistersAlarm1Red
	

	@Test
	public void testReadHoldingRegisters() {

		TCPMasterConnection tcpMasterConnection = null;
		
		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();
			
		}
		
		assertNotNull(tcpMasterConnection);

		int[] registerValuesToWrite = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 14 };

//		modbusTcpDriver.writeHoldingRegisters(tcpMasterConnection, 1, 9, registerValues);
		modbusTcpDriver.writeHoldingRegisters(1, 0, registerValuesToWrite);
		
//		int[] registerValues = modbusTcpDriver.readHoldingRegisters(tcpMasterConnection, 1, 9, 14);
//		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 9, 14);
		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 0, 20);

		modbusTcpDriver.closeConnection();

		assertEquals(1, registerValues[0]);
		assertEquals(2, registerValues[1]);
		assertEquals(3, registerValues[2]);
		assertEquals(4, registerValues[3]);
		assertEquals(5, registerValues[4]);
		assertEquals(6, registerValues[5]);
		assertEquals(7, registerValues[6]);
		assertEquals(8, registerValues[7]);
		assertEquals(9, registerValues[8]);
		assertEquals(10, registerValues[9]);
		assertEquals(11, registerValues[10]);
		assertEquals(12, registerValues[11]);
		assertEquals(0, registerValues[12]);
		assertEquals(14, registerValues[13]);
		
	} // end testReadHoldingRegisters


	@Test
	public void testWriteResetAsOne() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int[] registerValuesToWrite = new int[] { 1 };

		modbusTcpDriver.writeHoldingRegisters(1, 12, registerValuesToWrite);

		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 12, 1);

		modbusTcpDriver.closeConnection();

		assertEquals(1, registerValues[0]);

	} // end testWriteResetAsOne


	@Test
	public void testWriteResetAsZero() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int[] registerValuesToWrite = new int[] { 0 };

		modbusTcpDriver.writeHoldingRegisters(1, 12, registerValuesToWrite);

		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 12, 1);

		modbusTcpDriver.closeConnection();

		assertEquals(0, registerValues[0]);

	} // end testWriteMuteAsZero


	@Test
	public void testWriteMuteAsOne() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int[] registerValuesToWrite = new int[] { 1 };

		modbusTcpDriver.writeHoldingRegisters(1, 13, registerValuesToWrite);

		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 13, 1);

		modbusTcpDriver.closeConnection();

		assertEquals(1, registerValues[0]);

	} // end testWriteMuteAsOne


	@Test
	public void testWriteMuteAsZero() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int[] registerValuesToWrite = new int[] { 0 };

		modbusTcpDriver.writeHoldingRegisters(1, 13, registerValuesToWrite);

		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 13, 1);

		modbusTcpDriver.closeConnection();

		assertEquals(0, registerValues[0]);

	} // end testWriteMuteAsZero


	@Test
	public void testSingleWriteResetAsOne() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int registerValueToWrite = 1;

		modbusTcpDriver.writeSingleRegister(1, 12, registerValueToWrite);

		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 12, 1);

		modbusTcpDriver.closeConnection();

		assertEquals(1, registerValues[0]);

	} // end testSingleWriteResetAsOne


	@Test
	public void testSingleWriteResetAsZero() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int registerValueToWrite = 0;

		modbusTcpDriver.writeSingleRegister(1, 12, registerValueToWrite);

		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 12, 1);

		modbusTcpDriver.closeConnection();

		assertEquals(0, registerValues[0]);

	} // end testSingleWriteMuteAsZero


	@Test
	public void testSingleWriteMuteAsOne() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int registerValueToWrite = 1;

		modbusTcpDriver.writeSingleRegister(1, 13, registerValueToWrite);

		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 13, 1);

		modbusTcpDriver.closeConnection();

		assertEquals(1, registerValues[0]);

	} // end testSingleWriteMuteAsOne


	@Test
	public void testSingleWriteMuteAsZero() {

		TCPMasterConnection tcpMasterConnection= null;

		try {

			tcpMasterConnection = modbusTcpDriver.openConnection();

		} catch (Exception e) {

			e.printStackTrace();

		}

		assertNotNull(tcpMasterConnection);

		int registerValueToWrite = 0;

		modbusTcpDriver.writeSingleRegister(1, 13, registerValueToWrite);

		int[] registerValues = modbusTcpDriver.readHoldingRegisters(1, 13, 1);

		modbusTcpDriver.closeConnection();

		assertEquals(0, registerValues[0]);

	} // end testSingleWriteMuteAsZero

}
