package com.dimingo.modbus.hao.serial;

import static org.junit.Assert.*;

import net.wimpi.modbus.net.SerialConnection;
import net.wimpi.modbus.util.SerialParameters;

import org.junit.Before;
import org.junit.Test;

public class ModbusSerialDriverTest {
	
	private ModbusSerialDriver modbusSerialDriver;

	@Before
	public void setUp() {
		
		System.setProperty("gnu.io.rxtx.SerialPorts", "/dev/ttyS9");
		
		modbusSerialDriver = new ModbusSerialDriver();
		
		SerialParameters serialParameters = 
			new SerialParameters("/dev/ttyUSB1", 9600, 0, 0, 8, 1, 2, false, 500); // jamod 1.2-SNAPSHOT (500 = 5 sec)
//			new SerialParameters("/dev/ttyACM0", 9600, 0, 0, 8, 1, 2, false); // jamod 1.1 and 1.2-RC1
//		 	new SerialParameters("/dev/ttyS9", 9600, 0, 0, 8, 1, 2, false); // jamod 1.1 and 1.2-RC1
		assertNotNull(serialParameters);
		serialParameters.setEncoding("rtu");
		
		modbusSerialDriver.setSerialParameters(serialParameters);
		
	} // end  setUp
	
	
	@Test
	public void testSetMasterIdentifier() {
		
		modbusSerialDriver.setMasterIdentifier(7);
		
		assertEquals(7, modbusSerialDriver.getMasterIdentifier());
		
	} // end testSetMasterIdentifier
	

	@Test
	public void testOpenConnection() {
		
		SerialConnection serialConnection = null;
		
		try {
			
			serialConnection = modbusSerialDriver.openConnection();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		
		assertNotNull(serialConnection);
		
		modbusSerialDriver.closeConnection(serialConnection);
		
	} // end testOpenConnection

	
	@Test
	public void testCloseConnection() {
		
		SerialConnection serialConnection = null;
		
		try {
			
			serialConnection = modbusSerialDriver.openConnection();
			
		} catch (Exception e) {

			e.printStackTrace();
			
		}
		
		assertNotNull(serialConnection);
		
		modbusSerialDriver.closeConnection(serialConnection);
		
	} // end testCloseConnection
	

	@Test
	public void testReadInputRegisters() {
		fail("Not yet implemented");
	}

	@Test
	public void testWriteHoldingRegisters() {

		SerialConnection serialConnection = null;
		
		try {
			
			serialConnection = modbusSerialDriver.openConnection();
			
		} catch (Exception e) {

			e.printStackTrace();
			
		} 
		
		assertNotNull(serialConnection);
		
		int[] registerValues = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 14 };
		
		modbusSerialDriver.writeHoldingRegisters(serialConnection, 1, 9, registerValues);
		
		modbusSerialDriver.closeConnection(serialConnection);
		
	} // end testWriteHoldingRegisters
	

	@Test
	public void testReadHoldingRegisters() {
		
		SerialConnection serialConnection = null;
		
		try {
		
			serialConnection = modbusSerialDriver.openConnection();
			
		} catch (Exception e) {

			e.printStackTrace();
			
		}
		
		assertNotNull(serialConnection);
		
		int[] registerValues = modbusSerialDriver.readHoldingRegisters(serialConnection, 1, 9, 14);
		
		modbusSerialDriver.closeConnection(serialConnection);
		
		assertEquals(1, registerValues[0]);
		assertEquals(2, registerValues[1]);
		assertEquals(3, registerValues[2]);
		assertEquals(4, registerValues[3]);
		assertEquals(0, registerValues[4]);
		assertEquals(0, registerValues[5]);
		assertEquals(0, registerValues[6]);
		assertEquals(0, registerValues[7]);
		assertEquals(9, registerValues[8]);
		assertEquals(10, registerValues[9]);
		assertEquals(11, registerValues[10]);
		assertEquals(12, registerValues[11]);
		assertEquals(0, registerValues[12]);
		assertEquals(14, registerValues[13]);
		
	} // end testReadHoldingRegisters

}
