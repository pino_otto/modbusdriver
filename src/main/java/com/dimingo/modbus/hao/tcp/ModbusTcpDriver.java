package com.dimingo.modbus.hao.tcp;

import net.wimpi.modbus.*;
import net.wimpi.modbus.io.ModbusTCPTransaction;
import net.wimpi.modbus.msg.*;
import net.wimpi.modbus.net.TCPMasterConnection;
import net.wimpi.modbus.procimg.SimpleRegister;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.net.InetAddress;


public class ModbusTcpDriver implements IModbusTcpDriver, Serializable {

	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(ModbusTcpDriver.class);

	/** --------------------------------------------------------------------------------------------
	 * Spring injection (values and collaborators)
	 */

	private String slaveIpAddress = "192.168.0.173";

	private int slavePort = Modbus.DEFAULT_PORT;

	private int transactionDefaultMaxRetries = 10;

	private int transactionDefaultDelayMS = 500;


	/*
	 * Instance variables
	 */

	private TCPMasterConnection tcpMasterConnection = null;


	/**
	 * Default constructor
	 */
	public ModbusTcpDriver() {
		
	} // end default constructor

	
	/** --------------------------------------------------------------------------------------------
	 * Interface methods
	 */ 
	
	/**
	 * 
	 */
	public void setMasterIdentifier(int masterId) {
		
		ModbusCoupler.getReference().setUnitID(masterId);
		
	} // end setMasterIdentifier
	
	
	/**
	 * 
	 */
	public int getMasterIdentifier() {
		
		return ModbusCoupler.getReference().getUnitID();
		
	} // end getMasterIdentifier
	
	
	/**
	 * 
	 */
	public TCPMasterConnection openConnection() throws Exception {
		
		logger.debug("slave IP address = " + slaveIpAddress);
		logger.debug("slave Port       = " + slavePort);

		InetAddress slaveInetAddress = InetAddress.getByName(slaveIpAddress);

		// create a TCP connection to the slave
		tcpMasterConnection = new TCPMasterConnection(slaveInetAddress);
		tcpMasterConnection.setPort(slavePort);
		tcpMasterConnection.setTimeout(3000); // milliseconds
		
		try {
			
			// open the TCP connection
			tcpMasterConnection.connect();
			
		} catch (Exception e) {

			logger.error("Unable to open connection", e);
//			e.printStackTrace();
			
			// reset the TCP connection
			tcpMasterConnection = null;
			
			// rethrow the exception
			throw e;
			
		} // end catch
	    
	    return tcpMasterConnection;
		
	} // end openConnection


    /**
     *
     */
    public void closeConnection() {

        closeConnection(tcpMasterConnection);

    } // end closeConnection

	
	/**
	 * 
	 */
	public void closeConnection(TCPMasterConnection tcpMasterConnection) {

		if (tcpMasterConnection != null) {

			// close the TCP connection
			tcpMasterConnection.close();

			// reset the TCP connection variable
			this.tcpMasterConnection = null;
		}

	} // end closeConnection

	
	/**
	 * 
	 */
	public int[] readInputRegisters(int slaveId, int startAddress, int numberOfRegisters) {
		
		return readInputRegisters(tcpMasterConnection, slaveId, startAddress, numberOfRegisters);
		
	} // end readInputRegisters
	
	
	/**
	 * 
	 */
	public int[] readInputRegisters(TCPMasterConnection tcpMasterConnection, int slaveId, int startAddress, int numberOfRegisters) {
		
		// prepare an input register read request
		ReadInputRegistersRequest readRequest = new ReadInputRegistersRequest(startAddress, numberOfRegisters);
		readRequest.setUnitID(slaveId);
		if (Modbus.debug)
			logger.debug("Request: " + readRequest.getHexMessage());
		
		// prepare the transaction
		ModbusTCPTransaction transaction = new ModbusTCPTransaction(tcpMasterConnection);
		transaction.setRequest(readRequest);
		transaction.setRetries(transactionDefaultMaxRetries);
		transaction.setReconnecting(true); //TODO set to false (testing)?
		
		// execute the transaction
		try {
			transaction.execute();
		} catch (ModbusIOException e) {
			logger.error("ModbusIOException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		} catch (ModbusSlaveException e) {
			logger.error("ModbusSlaveException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		} catch (ModbusException e) {
			logger.error("ModbusException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		}
		
		// get the response
		logger.debug("Read input registers:");
		ReadInputRegistersResponse readResponse = (ReadInputRegistersResponse) transaction.getResponse();
		if (Modbus.debug) 
			logger.debug("Response: " + readResponse.getHexMessage());
		
		// prepare the array of register values to return
		int[] registerValues = new int[readResponse.getWordCount()];
		for (int n = 0; n < readResponse.getWordCount(); n++) {
			registerValues[n] = readResponse.getRegisterValue(n);
			logger.debug("Word " + n + "=" + registerValues[n]);
		} // end for

		return registerValues;
		
	} // end readInputRegisters

	
	/**
	 * 
	 */
	public void writeHoldingRegisters(int slaveId, int startAddress, int[] registerValues) {
		
		writeHoldingRegisters(tcpMasterConnection, slaveId, startAddress, registerValues);
		
	} // end writeHoldingRegisters
	
	
	/**
	 * 
	 */
	public void writeHoldingRegisters(TCPMasterConnection tcpMasterConnection, int slaveId, int startAddress, int[] registerValues) {

		// prepare a multiple holding register write request
		WriteMultipleRegistersRequest multipleWriteRequest = new WriteMultipleRegistersRequest();
		multipleWriteRequest.setUnitID(slaveId);

		// create the registers 
		SimpleRegister[] registers = new SimpleRegister[registerValues.length];
		
		// fill the register with the values
		for (int i = 0; i < registers.length; i++) {
			registers[i] = new SimpleRegister(registerValues[i]);
		}
		
		// put the registers in the write request
		multipleWriteRequest.setRegisters(registers);
		multipleWriteRequest.setReference(startAddress);
		if (Modbus.debug) 
			logger.debug("Request: " + multipleWriteRequest.getHexMessage());
		
		// prepare the transaction
		ModbusTCPTransaction transaction = new ModbusTCPTransaction(tcpMasterConnection);
		transaction.setRequest(multipleWriteRequest);
		transaction.setRetries(transactionDefaultMaxRetries);
		transaction.setReconnecting(true); //TODO set to false (testing)?
		
		// execute the transaction
		logger.debug("Before Write multiple holding registers, time = " + System.currentTimeMillis());
		try {
			transaction.execute();
		} catch (ModbusIOException e) {
			logger.error("ModbusIOException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		} catch (ModbusSlaveException e) {
			logger.error("ModbusSlaveException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		} catch (ModbusException e) {
			logger.error("ModbusException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		}
		logger.debug("After Write multiple holding registers, time = " + System.currentTimeMillis());
		
		// get the response
		WriteMultipleRegistersResponse multipleWriteResponse = (WriteMultipleRegistersResponse) transaction.getResponse();
		if (Modbus.debug) 
			logger.debug("Response: " + multipleWriteResponse.getHexMessage());
		
	} // end writeHoldingRegisters
	
	
	/**
	 * 
	 */
	public int[] readHoldingRegisters(int slaveId, int startAddress, int numberOfRegisters) {
		
		return readHoldingRegisters(tcpMasterConnection, slaveId, startAddress, numberOfRegisters);
		
	} // end readHoldingRegisters
	
	
	/**
	 * 
	 */
	public int[] readHoldingRegisters(TCPMasterConnection tcpMasterConnection, int slaveId, int startAddress, int numberOfRegisters) {
		
		// prepare a multiple holding register read request
		ReadMultipleRegistersRequest multipleReadRequest = new ReadMultipleRegistersRequest();
		multipleReadRequest.setUnitID(slaveId);
		multipleReadRequest.setReference(startAddress);
		multipleReadRequest.setWordCount(numberOfRegisters);
		if (Modbus.debug) 
			logger.debug("Request: " + multipleReadRequest.getHexMessage());
		
		// prepare the transaction
		ModbusTCPTransaction transaction = new ModbusTCPTransaction(tcpMasterConnection);
		transaction.setRequest(multipleReadRequest);
		transaction.setRetries(transactionDefaultMaxRetries);
		transaction.setReconnecting(true); //TODO set to false (testing)?
		
		// execute the transaction
		logger.debug("Before Read multiple holding registers, time = " + System.currentTimeMillis());
		try {
			transaction.execute();
		} catch (ModbusIOException e) {
			logger.error("ModbusIOException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		} catch (ModbusSlaveException e) {
			logger.error("ModbusSlaveException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		} catch (ModbusException e) {
			logger.error("ModbusException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		}
		logger.debug("After Read multiple holding registers, time = " + System.currentTimeMillis());
		
		ReadMultipleRegistersResponse multipleReadResponse = (ReadMultipleRegistersResponse) transaction.getResponse();
		if (Modbus.debug) 
			logger.debug("Response: " + multipleReadResponse.getHexMessage());
		
		// prepare the array of register values to return
		int[] registerValues = new int[multipleReadResponse.getWordCount()];
		for (int n = 0; n < multipleReadResponse.getWordCount(); n++) {
			registerValues[n] = multipleReadResponse.getRegisterValue(n);
			logger.debug("Word " + n + "=" + registerValues[n]);
		} // end for

		return registerValues;
		
	} // end readHoldingRegisters


	/**
	 *
	 */
	public void writeSingleRegister(int slaveId, int address, int registerValue) {

		writeSingleRegister(tcpMasterConnection, slaveId, address, registerValue);

	} // end writeSingleRegister


	/**
	 *
	 */
	public void writeSingleRegister(TCPMasterConnection tcpMasterConnection, int slaveId, int address, int registerValue) {

		// prepare a single holding register write request
		WriteSingleRegisterRequest singleWriteRequest = new WriteSingleRegisterRequest();
		singleWriteRequest.setUnitID(slaveId);

		// create the register with its value
		SimpleRegister register = new SimpleRegister(registerValue);

		// put the register in the write request
		singleWriteRequest.setRegister(register);
		singleWriteRequest.setReference(address);
		if (Modbus.debug)
			logger.debug("Request: " + singleWriteRequest.getHexMessage());

		// prepare the transaction
		ModbusTCPTransaction transaction = new ModbusTCPTransaction(tcpMasterConnection);
		transaction.setRequest(singleWriteRequest);
		transaction.setRetries(transactionDefaultMaxRetries);
		transaction.setReconnecting(true); //TODO set to false (testing)?

		// execute the transaction
		logger.debug("Before Write single holding register, time = " + System.currentTimeMillis());
		try {
			transaction.execute();
		} catch (ModbusIOException e) {
			logger.error("ModbusIOException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		} catch (ModbusSlaveException e) {
			logger.error("ModbusSlaveException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		} catch (ModbusException e) {
			logger.error("ModbusException", e);
			logger.debug(e.getMessage());
//			e.printStackTrace();
		}
		logger.debug("After Write single holding register, time = " + System.currentTimeMillis());

		// get the response
		WriteSingleRegisterResponse singleWriteResponse = (WriteSingleRegisterResponse) transaction.getResponse();
		if (Modbus.debug)
			logger.debug("Response: " + singleWriteResponse.getHexMessage());

	} // end writeSingleRegister



	/** --------------------------------------------------------------------------------------------
	 * Getters and Setters 
	 */

	public String getSlaveIpAddress() {
		return slaveIpAddress;
	}

	public void setSlaveIpAddress(String slaveIpAddress) {
		this.slaveIpAddress = slaveIpAddress;
	}

	public int getSlavePort() {
		return slavePort;
	}

	public void setSlavePort(int slavePort) {
		this.slavePort = slavePort;
	}

	public int getTransactionDefaultMaxRetries() {
		return transactionDefaultMaxRetries;
	}


	public void setTransactionDefaultMaxRetries(int transactionDefaultMaxRetries) {
		this.transactionDefaultMaxRetries = transactionDefaultMaxRetries;
	}

	public TCPMasterConnection getTcpMasterConnection() {
		return tcpMasterConnection;
	}

	public void setTcpMasterConnection(TCPMasterConnection tcpMasterConnection) {
		this.tcpMasterConnection = tcpMasterConnection;
	}

} // end class ModbusTcpDriver
