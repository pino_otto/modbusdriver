package com.dimingo.modbus.hao.tcp;

import net.wimpi.modbus.net.TCPMasterConnection;

public interface IModbusTcpDriver {
	
	void setMasterIdentifier(int masterId);
	
	int getMasterIdentifier();

    String getSlaveIpAddress();

    void setSlaveIpAddress(String slaveIpAddress);

    int getSlavePort();

    void setSlavePort(int slavePort);

	TCPMasterConnection openConnection() throws Exception;
	
	void closeConnection();
	void closeConnection(TCPMasterConnection tcpMasterConnection);

	int[] readInputRegisters(int slaveId, int startAddress, int numberOfRegisters);
	int[] readInputRegisters(TCPMasterConnection tcpMasterConnection, int slaveId, int startAddress, int numberOfRegisters);

	void writeHoldingRegisters(int slaveId, int startAddress, int[] registerValues);
	void writeHoldingRegisters(TCPMasterConnection tcpMasterConnection, int slaveId, int startAddress, int[] registerValues);

	int[] readHoldingRegisters(int slaveId, int startAddress, int numberOfRegisters);
	int[] readHoldingRegisters(TCPMasterConnection tcpMasterConnection, int slaveId, int startAddress, int numberOfRegisters);

	void writeSingleRegister(int slaveId, int address, int registerValue);
	void writeSingleRegister(TCPMasterConnection tcpMasterConnection, int slaveId, int address, int registerValue);

}
