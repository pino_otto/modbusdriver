package com.dimingo.modbus.hao.serial;

import java.io.Serializable;

import org.apache.log4j.Logger;

import net.wimpi.modbus.Modbus;
import net.wimpi.modbus.ModbusCoupler;
import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.ModbusIOException;
import net.wimpi.modbus.ModbusSlaveException;
import net.wimpi.modbus.io.ModbusSerialTransaction;
import net.wimpi.modbus.msg.ReadInputRegistersRequest;
import net.wimpi.modbus.msg.ReadInputRegistersResponse;
import net.wimpi.modbus.msg.ReadMultipleRegistersRequest;
import net.wimpi.modbus.msg.ReadMultipleRegistersResponse;
import net.wimpi.modbus.msg.WriteMultipleRegistersRequest;
import net.wimpi.modbus.msg.WriteMultipleRegistersResponse;
import net.wimpi.modbus.net.SerialConnection;
import net.wimpi.modbus.procimg.SimpleRegister;
import net.wimpi.modbus.util.SerialParameters;


public class ModbusSerialDriver implements IModbusSerialDriver, Serializable {

	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(ModbusSerialDriver.class);
	
	/** --------------------------------------------------------------------------------------------
	 * Spring injection (values and collaborators)
	 */ 
	
	private int transactionDefaultMaxRetries = 10;
	
	private int transactionDefaultDelayMS = 500;
	
	private SerialParameters serialParameters;
	
	
	/*
	 * Instance variables
	 */
	
	private SerialConnection serialConnection = null;
	
	
	/**
	 * Default constructor
	 */
	public ModbusSerialDriver() {
		
	} // end default constructor

	
	/** --------------------------------------------------------------------------------------------
	 * Interface methods
	 */ 
	
	/**
	 * 
	 */
	public void setMasterIdentifier(int masterId) {
		
		ModbusCoupler.getReference().setUnitID(masterId);
		
	} // end setMasterIdentifier
	
	
	/**
	 * 
	 */
	public int getMasterIdentifier() {
		
		return ModbusCoupler.getReference().getUnitID();
		
	} // end getMasterIdentifier
	
	
	/**
	 * 
	 */
	public SerialConnection openConnection() throws Exception {
		
		logger.debug("serialParameters = " + serialParameters);
		logger.debug("m_PortName       = " + serialParameters.getPortName());
		logger.debug("m_BaudRate       = " + serialParameters.getBaudRate());
		logger.debug("m_FlowControlIn  = " + serialParameters.getFlowControlIn());
		logger.debug("m_FlowControlOut = " + serialParameters.getFlowControlOut());
		logger.debug("m_Databits       = " + serialParameters.getDatabits());
		logger.debug("m_Stopbits       = " + serialParameters.getStopbits());
		logger.debug("m_Parity         = " + serialParameters.getParity());
		logger.debug("m_Encoding       = " + serialParameters.getEncoding());
		logger.debug("m_ReceiveTimeout = " + serialParameters.getReceiveTimeout());
		
		// create a serial connection
		serialConnection = new SerialConnection(serialParameters);
		
		try {
			
			// open the serial connection
			serialConnection.open();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			// reset the serial connection
			serialConnection = null;
			
			// rethrow the exception
			throw e;
			
		} // end catch
	    
	    return serialConnection;
		
	} // end openConnection


    /**
     *
     */
    public void closeConnection() {

        closeConnection(serialConnection);

    } // end closeConnection
	
	
	/**
	 * 
	 */
	public void closeConnection(SerialConnection serialConnection) {
		
		// close the serial connection
		serialConnection.close();
		
		// reset the serial connection variable
		this.serialConnection = null;
		
		
	} // end closeConnection

	
	/**
	 * 
	 */
	public int[] readInputRegisters(int slaveId, int startAddress, int numberOfRegisters) {
		
		return readInputRegisters(serialConnection, slaveId, startAddress, numberOfRegisters);
		
	} // end readInputRegisters
	
	
	/**
	 * 
	 */
	public int[] readInputRegisters(SerialConnection serialConnection,
			int slaveId, int startAddress, int numberOfRegisters) {
		
		// prepare an input register read request
		ReadInputRegistersRequest readRequest = new ReadInputRegistersRequest(startAddress, numberOfRegisters);
		readRequest.setUnitID(slaveId);
		readRequest.setHeadless();
		if (Modbus.debug) 
			logger.debug("Request: " + readRequest.getHexMessage());
		
		// prepare the transaction
		ModbusSerialTransaction transaction = new ModbusSerialTransaction(serialConnection);
		transaction.setRequest(readRequest);
		transaction.setRetries(transactionDefaultMaxRetries);
		transaction.setTransDelayMS(transactionDefaultDelayMS);
		
		// execute the transaction
		try {
			transaction.execute();
		} catch (ModbusIOException e) {
			e.printStackTrace();
		} catch (ModbusSlaveException e) {
			e.printStackTrace();
		} catch (ModbusException e) {
			e.printStackTrace();
		}
		
		// get the response
		logger.debug("Read input registers:");
		ReadInputRegistersResponse readResponse = (ReadInputRegistersResponse) transaction.getResponse();
		if (Modbus.debug) 
			logger.debug("Response: " + readResponse.getHexMessage());
		
		// prepare the array of register values to return
		int[] registerValues = new int[readResponse.getWordCount()];
		for (int n = 0; n < readResponse.getWordCount(); n++) {
			registerValues[n] = readResponse.getRegisterValue(n);
			logger.debug("Word " + n + "=" + registerValues[n]);
		} // end for

		return registerValues;
		
	} // end readInputRegisters

	
	/**
	 * 
	 */
	public void writeHoldingRegisters(int slaveId, int startAddress, int[] registerValues) {
		
		writeHoldingRegisters(serialConnection, slaveId, startAddress, registerValues);
		
	} // end writeHoldingRegisters
	
	
	/**
	 * 
	 */
	public void writeHoldingRegisters(SerialConnection serialConnection,
			int slaveId, int startAddress, int[] registerValues) {

		// prepare a multiple holding register write request
		WriteMultipleRegistersRequest multipleWriteRequest = new WriteMultipleRegistersRequest();
		multipleWriteRequest.setUnitID(slaveId);
		multipleWriteRequest.setHeadless();

		// create the registers 
		SimpleRegister[] registers = new SimpleRegister[registerValues.length];
		
		// fill the register with the values
		for (int i = 0; i < registers.length; i++) {
			registers[i] = new SimpleRegister(registerValues[i]);
		}
		
		// put the registers in the write request
		multipleWriteRequest.setRegisters(registers);
		multipleWriteRequest.setReference(startAddress);
		if (Modbus.debug) 
			logger.debug("Request: " + multipleWriteRequest.getHexMessage());
		
		// prepare the transaction
		ModbusSerialTransaction transaction = new ModbusSerialTransaction(serialConnection);
		transaction.setRequest(multipleWriteRequest);
		transaction.setRetries(transactionDefaultMaxRetries);
		transaction.setTransDelayMS(transactionDefaultDelayMS);
		
		// execute the transaction
		logger.debug("Before Write multiple holding registers, time = " + System.currentTimeMillis());
		try {
			transaction.execute();
		} catch (ModbusIOException e) {
			e.printStackTrace();
		} catch (ModbusSlaveException e) {
			e.printStackTrace();
		} catch (ModbusException e) {
			e.printStackTrace();
		}
		logger.debug("After Write multiple holding registers, time = " + System.currentTimeMillis());
		
		// get the response
		WriteMultipleRegistersResponse multipleWriteResponse = (WriteMultipleRegistersResponse) transaction.getResponse();
		if (Modbus.debug) 
			logger.debug("Response: " + multipleWriteResponse.getHexMessage());
		
	} // end writeHoldingRegisters
	
	
	/**
	 * 
	 */
	public int[] readHoldingRegisters(int slaveId, int startAddress, int numberOfRegisters) {
		
		return readHoldingRegisters(serialConnection, slaveId, startAddress, numberOfRegisters);
		
	} // end readHoldingRegisters
	
	
	/**
	 * 
	 */
	public int[] readHoldingRegisters(SerialConnection serialConnection,
			int slaveId, int startAddress, int numberOfRegisters) {
		
		// prepare a multiple holding register read request
		ReadMultipleRegistersRequest multipleReadRequest = new ReadMultipleRegistersRequest();
		multipleReadRequest.setUnitID(slaveId);
		multipleReadRequest.setHeadless();
		multipleReadRequest.setReference(startAddress);
		multipleReadRequest.setWordCount(numberOfRegisters);
		if (Modbus.debug) 
			logger.debug("Request: " + multipleReadRequest.getHexMessage());
		
		// prepare the transaction
		ModbusSerialTransaction transaction = new ModbusSerialTransaction(serialConnection);
		transaction.setRequest(multipleReadRequest);
		transaction.setRetries(transactionDefaultMaxRetries);
		transaction.setTransDelayMS(transactionDefaultDelayMS);
		
		// execute the transaction
		logger.debug("Before Read multiple holding registers, time = " + System.currentTimeMillis());
		try {
			transaction.execute();
		} catch (ModbusIOException e) {
			e.printStackTrace();
		} catch (ModbusSlaveException e) {
			e.printStackTrace();
		} catch (ModbusException e) {
			e.printStackTrace();
		}
		logger.debug("After Read multiple holding registers, time = " + System.currentTimeMillis());
		
		ReadMultipleRegistersResponse multipleReadResponse = (ReadMultipleRegistersResponse) transaction.getResponse();
		if (Modbus.debug) 
			logger.debug("Response: " + multipleReadResponse.getHexMessage());
		
		// prepare the array of register values to return
		int[] registerValues = new int[multipleReadResponse.getWordCount()];
		for (int n = 0; n < multipleReadResponse.getWordCount(); n++) {
			registerValues[n] = multipleReadResponse.getRegisterValue(n);
			logger.debug("Word " + n + "=" + registerValues[n]);
		} // end for

		return registerValues;
		
	} // end readHoldingRegisters

	
	/** --------------------------------------------------------------------------------------------
	 * Getters and Setters 
	 */ 

	public int getTransactionDefaultMaxRetries() {
		return transactionDefaultMaxRetries;
	}


	public void setTransactionDefaultMaxRetries(int transactionDefaultMaxRetries) {
		this.transactionDefaultMaxRetries = transactionDefaultMaxRetries;
	}


	public int getTransactionDefaultDelayMS() {
		return transactionDefaultDelayMS;
	}


	public void setTransactionDefaultDelayMS(int transactionDefaultDelayMS) {
		this.transactionDefaultDelayMS = transactionDefaultDelayMS;
	}


	public SerialParameters getSerialParameters() {
		return serialParameters;
	}


	public void setSerialParameters(SerialParameters serialParameters) {
		this.serialParameters = serialParameters;
	}


	public SerialConnection getSerialConnection() {
		return serialConnection;
	}


	public void setSerialConnection(SerialConnection serialConnection) {
		this.serialConnection = serialConnection;
	}
	

} // end class ModbusSerialDriver
