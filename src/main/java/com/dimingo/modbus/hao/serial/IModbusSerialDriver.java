package com.dimingo.modbus.hao.serial;

import net.wimpi.modbus.net.SerialConnection;
import net.wimpi.modbus.util.SerialParameters;

public interface IModbusSerialDriver {
	
	void setMasterIdentifier(int masterId);
	
	int getMasterIdentifier();
	
	SerialParameters getSerialParameters();
	
	SerialConnection getSerialConnection();
	
	SerialConnection openConnection() throws Exception;
	
	void closeConnection();
	void closeConnection(SerialConnection serialConnection);

	int[] readInputRegisters(int slaveId, int startAddress, int numberOfRegisters);
	int[] readInputRegisters(SerialConnection serialConnection, int slaveId, int startAddress, int numberOfRegisters);

	void writeHoldingRegisters(int slaveId, int startAddress, int[] registerValues);
	void writeHoldingRegisters(SerialConnection serialConnection, int slaveId, int startAddress, int[] registerValues);

	int[] readHoldingRegisters(int slaveId, int startAddress, int numberOfRegisters);
	int[] readHoldingRegisters(SerialConnection serialConnection, int slaveId, int startAddress, int numberOfRegisters);

}
